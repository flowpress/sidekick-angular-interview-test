#Sidekick Angular Interview Test Instructions

1. Install Angular.js locally and add it in index.html
2. Make an Angular controller, take the `div.fruit` item and repeat it for every item in data.json (Don't worry about making an HTTP request for data.json, you can just paste it into the controller hardcoded)
3. If the fruit is delicious, colour the text green, otherwise color the text red.
4. Add an alert on clicking that reports which fruit you clicked on
5. Create an Angular service that contains the fruits data and make it available to your controller's scope
6. Create a second list thats prints the opposite color for each fruit item.
7. Make a way to visually toggle between the two lists. i.e. you shouldn't see both of the lists at once